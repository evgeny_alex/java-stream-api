package lesson6.hashmap;

import java.util.HashMap;
import java.util.Map;

public class App {

    public static void main(String[] args) {
        Map<String, Object> map = new HashMap<>();

        map.put("test", 1);
        map.put("test2", 2);

        System.out.println(hash("test"));
        System.out.println(hash("test2"));

        System.out.println(index("test"));
        System.out.println(index("test2"));

        System.out.println(map);
    }

    static int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    static int index(Object key) {
        return (16 - 1) & hash(key);
    }
}
