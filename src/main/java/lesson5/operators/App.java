package lesson5.operators;

public class App {
    public static void main(String[] args) {
        // Арифметические операции
//        int a = 5 + 6;
//        int b = 40;
//        int c = a + b;
////        System.out.println(c);
//
//        int x = 10;
////        System.out.println(++x); // Сначала используется в выражении, а потом увеличивается
////        System.out.println(x * 2);
//
//        int y = 10;
//        System.out.println(--y); // Сначала увеличивается, а потом используется в выражении
//        System.out.println(y * 2);
//
////        // Операторы сравнения
//        int a2 = 10;
//        int b2 = 20;
////        System.out.println("a == b = " + (a2 == b2) );
////        System.out.println("a != b = " + (a2 != b2) );
//        System.out.println("a > b = " + (a2 > b2) );
//        System.out.println("a < b = " + (a2 < b2) );
//        System.out.println("b >= a = " + (b2 >= a2) );
//        System.out.println("b <= a = " + (b2 <= a2) );
//
//        // Побитовые операции
        int a3 = -60;    /* 60 = 0011 1100 */
        int b3 = 13;    /* 13 = 0000 1101 */
        int c3 = 0;

//        c3 = a3 & b3;       /* 12 = 0000 1100 */
//        System.out.println("a & b = " + Integer.toBinaryString(c3));
//
//        c3 = a3 | b3;       /* 61 = 0011 1101 */
//        System.out.println("a | b = " + c3 );
//
//        c3 = a3 ^ b3;       /* 49 = 0011 0001 */
//        System.out.println("a ^ b = " + c3 );
//
//        c3 = ~a3;          /*-61 = 1100 0011 */
//        System.out.println("~a = " + c3 );
//
//                            /* 60 = 0011 1100 */
//        c3 = a3 << 2;       /*240 = 1111 0000 */
//        System.out.println("a << 2 = " + Integer.toBinaryString(c3) );
//

//        c3 = a3 >> 3;
//        System.out.println("a >> 1  = " + Integer.toBinaryString(c3));
////
//        c3 = a3 >>> 2;     /* 215 = 0000 1111 */
//        System.out.println("a >>> 2 = " + Integer.toBinaryString(c3));
//
//        // Логические операции
//        boolean a4 = true;
//        boolean b4 = false;
//
//        System.out.println("a && b = " + (a4 && b4));
//        System.out.println("a || b = " + (a4 || b4));
//        System.out.println("!(a && b) = " + !(a4 && b4));
//
//        // Присваивание
//        int a = 10;
//        int b = 20;
//        int c = 0;

//        c = a + b;
//        System.out.println("c = a + b = " + c );
//
//        c += a ;
//        System.out.println("c += a  = " + c );
//
//        c -= a ;
//        System.out.println("c -= a = " + c );
//
//        c *= a ;
//        System.out.println("c *= a = " + c );
//
//        a = 10;
//        c = 15;
//        c /= a ;
//        System.out.println("c /= a = " + c );
//
//        a = 10;
//        c = 15;
//        c %= a;
//        System.out.println("c %= a  = " + c );
//
//        c <<= 2 ;
//        int c2 = c << 2;
//        System.out.println("c <<= 2 = " + c );
//        System.out.println("c2 = c << 2 = " + c2 );
//
//        c >>= 2 ;
//        System.out.println("c >>= 2 = " + c );
//
//        c >>= 2 ;
//        System.out.println("c >>= a = " + c );
//
//        c &= a ;
//        System.out.println("c &= 2  = " + c );
//
//        c ^= a ;
//        System.out.println("c ^= a   = " + c );
//
//        c |= a ;
//        System.out.println("c |= a   = " + c );
//
//        // Тернарный оператор
//        int a, b;
//        a = 10;
//        b = (a == 1) ? 20 : 30;
//        System.out.println("Значение b: " + b);
//
//        b = (a == 10) ? 20 : 30;
//        System.out.println("Значение b: " + b);

        // instanceof
//        String name = "Jack";
//        boolean result = name instanceof String;
//        System.out.println( result );
//
//        // Конструкция if/else
//        int num1 = 6;
//        int num2 = 4;
//        if (num1 > num2) {
//            System.out.println("Первое число больше второго");
//        } else {
//            System.out.println("Первое число меньше второго");
//        }
//
//        // Конструкция switch
        int color = 1;
        switch (color) {
            case 1 -> {
                System.out.println("число равно 1");
                color = 8;
            }
            case 8 -> System.out.println("число равно 8");
            case 9 -> System.out.println("число равно 9");
            default -> System.out.println("число не равно 1, 8, 9");
        }
    }


}
