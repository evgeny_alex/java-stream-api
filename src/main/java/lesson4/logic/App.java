package lesson4.logic;

import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.function.Predicate;

public class App {

    static List<String> list = new ArrayList<>(List.of("1", "2", "3"));

    public static void main(String[] args) {
        System.out.println(Arrays.toString(plusOne(new int[]{1, 2, 4, 9, 9, 9})));
    }

    public static int[] plusOne(int[] digits) {
        boolean enlargeArray = false;
        for (int i = digits.length - 1; i >= 0; i--) {
            if (digits[i] == 9) {
                digits[i] = 0;
                enlargeArray = true;
            } else {
                digits[i]++;
                enlargeArray = false;
                break;
            }
        }
        if (enlargeArray) {
            int[] newDigits = new int[digits.length + 1];
            newDigits[0] = 1;
            return newDigits;
        }
        return digits;
    }

    public static int removeElement(int[] nums, int val) {
        int i = 0;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val) {
                nums[i] = nums[j];
                i++;
            }
        }
        return i;
    }

    public static int strStr(String haystack, String needle) {
        if (needle == null || needle.equals("")) {
            return 0;
        }
        for (int i = 0; i < haystack.length(); i++) {
            boolean found = false;
            for (int j = 0; j < needle.length(); j++) {
                if ((i + j) < haystack.length()) {
                    if (haystack.charAt(i + j) == needle.charAt(j)) {
                        found = true;
                    } else {
                        found = false;
                    }
                } else {
                    found = false;
                }
            }
            if (found) {
                return i;
            }
        }
        return -1;
    }

    public static int removeElement2(int[] nums, int val) {
        int i = 0;
        int n = nums.length;
        while (i < n) {
            if (nums[i] == val) {
                nums[i] = nums[n - 1];
                // reduce array size by one
                n--;
            } else {
                i++;
            }
        }
        return n;
    }

    public static boolean retainAll(final Collection<?> c) {
        // BEGIN (write your solution here)
        for (final String item : list){
            if(!c.contains(item)) {
                list.remove(item);
            }
        }
        return true;
        // END
    }

    public static int convertNumber(int number) {
        var sum = 0;
        for (int i = number; i > 0; i /= 10) {
            sum += i % 10;
        }
        return sum;
    }

    public static boolean isOneDigitNumber(int number) {
        if (number < 10) {
            return true;
        }
        return false;
    }

    public static int addDigits(int num) {
        while (!isOneDigitNumber(num)) {
            num = convertNumber(num);
        }
        return num;
    }

    public static int compute(String s1, String s2, Integer value) {
        if (s1 == s2) {
            return 0;
        }
        if (value == null) {
            return -1;
        }
        return value;
    }

    public static void logicAnd() {
        boolean a = false;
        boolean b = true;
        boolean c = false;

        boolean d = a && b;
        System.out.println(d); // false

        boolean e = a && b && !b; // false

        System.out.println(e);

        boolean f = a && b && c && !a && !b && !c;

        System.out.println(f); // false
    }

    public static void logicOr() {
        boolean a = false;
        boolean b = true;
        boolean c = false;

        boolean d = a && b;

//        System.out.println(d); // false

        boolean e = a || b || !b;

//        System.out.println(e); // true

        boolean f = a && b || (c && (!a || !b) || !c); // false ��� ()

        System.out.println(f);
    }

    public static void logicXOR() {
        boolean a = false;
        boolean b = true;
        boolean c = false;

        boolean d = a ^ b;
        System.out.println(d);

        boolean e = a ^ b ^ !b;
        System.out.println(e);

        boolean f = a && (b ^ c || (!a ^ !b ^ !c));
        System.out.println(f);
    }

    public static void logicExamples() {
        boolean a = false;
        boolean b = true;
        boolean c = false;

        boolean d = (a | b) & (2 < 6) ^ false; // true

        System.out.println(d);

        boolean f = !((a | b ^ c) & (c & (6 > 8)) ^ true);

        System.out.println(f);

        boolean e = (a && b) & (2 < 6) & !false;

        System.out.println(e);
    }

    public static void predicate() {
        Predicate<String> predicate1 = (s) -> s.startsWith("a");
        Predicate<String> predicate2 = (s) -> s.endsWith("l");

        System.out.println(predicate1.test("all"));
        System.out.println(predicate1.test("not all"));
        System.out.println(predicate1.negate().and(predicate2).test("not all"));
    }
}

record Student(String name) {

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Group implements Iterable<Student> { //����� Group ������������ �� ���������

    private Student[] students; //���� ������ G ������ ���������

    public Group(final Student[] students) {  //����������� ������ Group � �������� ����������, ��� �������� ������ ������� ����� ������
        this.students = students;  //������ ��������� �� ������ ������� ������ (����� ��� ����)= ������ ��������� ����� G ������
    }

    public final Iterator<Student> iterator() { //����� iterator ������� ���������� ������ ������ Iterator(�� ���������)
        return new StudentsIterator(students); //������� ����� ������ ������ SI ������� ���� ������ ��������� ������� ������ G
    }

    private static class StudentsIterator implements Iterator<Student> { // ����� SI ������������ �� ���������, ������ ������� ������ Group
        // BEGIN (write your solution here)
        private StudentsIterator(final Student[] students){ //����������� ������ SI
            this.students = students;  //������ ��������� �� ������ ������� ������ (����� ��� ����)= ������ ��������� ����� SI ������
        }

        private Student[] students; //������ ��������� ���, ���, ����� 2

        private int index = 0; // =1 ��������� ������� ��� (��� ���� �������� =2)

        public boolean hasNext() {
            return index < this.students.length; // true ������ ��� 1 ������ ����� ������� 2 (��� ���� ������ false)
        }

        public Student next() throws NoSuchElementException {
            final int currentIndex = index; // current =1 (��� ���� ������ =2)
            if (hasNext()) {  // hasnext true (��� ���� �������� false � �� �������� � else)
                index = index+1;  // index=2
                return this.students[currentIndex]; // ���������� ���.
            } else {
                throw new NoSuchElementException(); // ����������� ���������� ��� hasnext=false ��� index=2 ��� ������� ��� ���
            }
        }
        // END
    }
}
