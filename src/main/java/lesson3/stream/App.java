package lesson3.stream;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class App {
    public static void main(String[] args) {
        Task6.doTask();
    }
}

class Task1 {
    public static void doTask() {
        List<Integer> intList = new Random()
                .ints(5, 1, 11)
                .boxed().toList();

        int countEven = 0;
        for (Integer number : intList) {
            if (number % 2 == 0) {
                countEven++;
            }
        }

        System.out.println(intList);
        System.out.println(countEven);

        int count = (int) intList.stream().filter(n -> n % 2 == 0).count();

        System.out.println(count);
    }
}

class Task2 {
    public static void doTask() {
        List<String> names = List.of("Jack", "John", "Mike", "Laura", "Jim");

        List<String> resultNames = new ArrayList<>();
        for (String name : names) {
            if (name.startsWith("J")) {
                resultNames.add(name);
            }
        }

        System.out.println(resultNames);

        List<String> resultNamesStream = names.stream().filter(s -> s.startsWith("J")).toList();
        System.out.println(resultNamesStream);
    }
}

class Order {

    private Long id;
    private Date orderDate;
    private List<Product> products;

    public Order(Long id, Date orderDate, List<Product> products) {
        this.id = id;
        this.orderDate = orderDate;
        this.products = products;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderDate=" + orderDate +
                ", products=" + products +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

class Product {
    private String name;
    private String category;
    private Double price;

    public Product(String name, String category, Double price) {
        this.name = name;
        this.category = category;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", category='" + category + '\'' +
                ", price=" + price +
                '}';
    }
}

class Task3 {

    public static List<Order> initOrders() {
        ZoneId z = ZoneId.of("America/Montreal");

        Product product1 = new Product("Candy", "Baby", 2.7);
        Product product2 = new Product("Book", "Adult", 5.1);
        Product product3 = new Product("Car", "Baby", 7.4);
        Product product4 = new Product("Table", "Adult", 12.3);
        Product product5 = new Product("Constructor", "Baby", 23.8);

        Order order1 = new Order(1L, Date.from(LocalDate.of(2021, 3, 15).atStartOfDay(z).toInstant()),
                List.of(product1, product3, product5));

        Order order2 = new Order(2L, Date.from(LocalDate.of(2022, 3, 15).atStartOfDay(z).toInstant()),
                List.of(product2));

        Order order3 = new Order(3L, Date.from(LocalDate.of(2020, 3, 15).atStartOfDay(z).toInstant()),
                List.of(product2, product4));

        Order order4 = new Order(4L, Date.from(LocalDate.of(2015, 3, 15).atStartOfDay(z).toInstant()),
                List.of(product3, product4));

        Order order5 = new Order(5L, Date.from(LocalDate.of(2021, 3, 15).atStartOfDay(z).toInstant()),
                List.of(product1, product5, product2, product4));

        return List.of(order1, order2, order3, order4, order5);
    }

    public static void doTask() {

        List<Order> list = Task3.initOrders();

        String categoryBaby = "Baby";

        List<Order> resultWithoutStream = new ArrayList<>();

        boolean found = false;
        for (Order order : list) {
            for (Product product : order.getProducts()) {
                if (product.getCategory().equalsIgnoreCase(categoryBaby)) {
                    found = true;
                    break;
                }
            }
            if (found) {
                resultWithoutStream.add(order);
            }
            found = false;
        }

        System.out.println(resultWithoutStream);

        List<Order> result = list.stream()
                .filter(o ->
                        o.getProducts()
                                .stream()
                                .anyMatch(p -> p.getCategory().equalsIgnoreCase(categoryBaby))
                ).toList();

        System.out.println(result);
    }
}

class Task4 {
    public static void doTask() {
        ZoneId z = ZoneId.of("America/Montreal");

        List<Order> list = Task3.initOrders();

        List<Order> resultWithoutStream = new ArrayList<>();

        Set<Product> filteredProducts = new HashSet<>();
        for (Order order : list) {
            if (order.getOrderDate().equals(Date.from(LocalDate.of(2021, 3, 15).atStartOfDay(z).toInstant()))) {
                System.out.println(order);
                filteredProducts.addAll(order.getProducts());
            }
        }
        System.out.println(filteredProducts);

        List<Product> result = list
                .stream()
                .filter(o -> o.getOrderDate().equals(Date.from(LocalDate.of(2021, 3, 15).atStartOfDay(z).toInstant())))
                .peek(System.out::println)
                .flatMap(o -> o.getProducts().stream())
                .distinct()
                .collect(Collectors.toList());

        System.out.println(result);
    }
}

class Task5 {
    public static void doTask() {
        ZoneId z = ZoneId.of("America/Montreal");
        List<Order> list = Task3.initOrders();

        Double resultWithoutStream = 0.0;
        int countProducts = 0;
        for (Order order : list) {
            if (order.getOrderDate().equals(Date.from(LocalDate.of(2021, 3, 15).atStartOfDay(z).toInstant()))) {
                for (Product product : order.getProducts()) {
                    resultWithoutStream += product.getPrice();
                    countProducts++;
                }
            }
        }
        System.out.println(resultWithoutStream / countProducts);

        Double result = list
                .stream()
                .filter(o -> o.getOrderDate().equals(Date.from(LocalDate.of(2021, 3, 15).atStartOfDay(z).toInstant())))
                .flatMap(o -> o.getProducts().stream())
                .mapToDouble(Product::getPrice)
                .average().getAsDouble();

        System.out.println(result);
    }
}

class Task6 {
    public static void doTask() {

        List<Order> list = Task3.initOrders();

        Map<Long, Integer> resultWithoutStream = new HashMap<>();
        for (Order order : list) {
            resultWithoutStream.put(order.getId(), order.getProducts().size());
        }
        System.out.println(resultWithoutStream);

        Map<Long, Integer> result = list
                .stream()
                .collect(
                        Collectors.toMap(
                                order -> order.getId(),
                                order -> order.getProducts().size()
                        )
                );

        System.out.println(result);
    }
}


